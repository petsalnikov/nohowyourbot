FROM python:3.9.1-alpine

COPY . .
RUN pip3 install -r requirements.txt
CMD python main.py