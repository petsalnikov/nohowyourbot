import functools
import threading
import time

import telebot

bot = telebot.TeleBot("5657310829:AAEl7Qj1Q2UHOeuOZF2CEYEcrGUrGjVJYOE")

count = 0
max_count = 15
timeout_minutes = 5
timeout_start = -1


def roll(x):
    if x == 1:
        return "остался 1 ролл"
    if x in (2, 3, 4):
        return "осталось {} ролла".format(x)
    return "осталось {} роллов".format(x)


msg_to_delete = set()


def delete_message(r):
    def del_(r_):
        time.sleep(3)
        bot.delete_message(r_.chat.id, r_.id)

    t = threading.Thread()
    t.run = functools.partial(del_, r)
    t.start()


@bot.message_handler(func=lambda m: m.via_bot and m.via_bot.username.lower() == "howyourbot")
def remove_message(m: telebot.types.Message):
    global count, max_count, timeout_minutes, timeout_start
    r = None
    msg_to_delete.add(m.text)
    if count < max_count:
        count += 1
        if count == max_count:
            bot.reply_to(m, "Всё пошел нахуй со своим ботом на {} минут".format(timeout_minutes))
            timeout_start = time.time()
            count += 1
        else:
            r = bot.reply_to(m, "У тебя {} бота до таймаута".format(roll(max_count - count)))
    elif time.time() < timeout_start + timeout_minutes * 60:
        bot.delete_message(m.chat.id, m.id)
    else:
        count = 0
        r = bot.reply_to(m, "У тебя {} бота до таймаута".format(roll(max_count - count)))
    if r:
        delete_message(r)


@bot.message_handler(func=lambda m: m.text in msg_to_delete)
def delete_msg_from_text(m: telebot.types.Message):
    global timeout_start, timeout_minutes
    r = bot.reply_to(m, "ахаха не читерить")
    bot.delete_message(m.chat.id, m.id)
    delete_message(r)


@bot.message_handler(commands=["add_keyboard"])
def add_keyboard(m: telebot.types.Message):
    keyboard = telebot.types.ReplyKeyboardMarkup()
    keyboard.add("сломать колени")
    bot.send_message(m.chat.id, "клавиатура добавлена", reply_markup=keyboard)


@bot.message_handler(commands=["remove_keyboard"])
def remove_keyboard(m: telebot.types.Message):
    rem_keyboard = telebot.types.ReplyKeyboardRemove()
    bot.send_message(m.chat.id, "клавиатура удалена", reply_markup=rem_keyboard)


commands = [telebot.types.BotCommand("add_keyboard", "создать клавиаутуру"),
            telebot.types.BotCommand("remove_keyboard", "убрать клавиаутуру")]
bot.set_my_commands(commands)
bot.polling()
